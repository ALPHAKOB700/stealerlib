# StealerLib

This library is designed to steal password on some web browsers and apps.

## Infos

**For now it support :**

**Browsers :**
- Chrome (Chrome, Chromium, Opera, Vivaldi, Brave, Torch, Comodo, Xpom, Orbitum, Kometa, Amigo and Nichrome) *²*
- Firefox *²*
- IE10 / Edge 

Yandex Browser not fully supported for now only Username and Cookies works.

**Apps :**
- NordVPN (User Config File)
- Coinomi (User Wallet File)


*² Credential and Cookies*
